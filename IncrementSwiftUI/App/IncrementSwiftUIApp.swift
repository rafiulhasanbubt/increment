//
//  IncrementSwiftUIApp.swift
//  IncrementSwiftUI
//
//  Created by rafiul hasan on 12/20/20.
//

import SwiftUI

@main
struct IncrementSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            LandingView()
        }
    }
}
