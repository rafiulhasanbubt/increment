//
//  LandingView.swift
//  IncrementSwiftUI
//
//  Created by rafiul hasan on 12/20/20.
//

import SwiftUI

struct LandingView: View {
    //MARK: Properties
    @State private var isActive: Bool = false
    
    //MARK: Body
    var body: some View {
        NavigationView {
            GeometryReader { proxy in
                VStack {
                    Spacer().frame(height: proxy.size.height * 0.18)
                    
                    Text("Increment")
                        .font(.system(size: 60, weight: .medium))
                        .foregroundColor(.white)
                    
                    Spacer()//.frame(height: proxy.size.height * 0.18)
                    
                    NavigationLink(destination: CreateView(), isActive: $isActive) {
                        Button(action: {
                            // Action come here
                            self.isActive = true
                        }) {
                            HStack(spacing: 15.0) {
                                Spacer()
                                Image(systemName: "plus.circle")
                                    .font(.system(size: 24, weight: .semibold))
                                    .foregroundColor(.white)
                                
                                Text("Create a Challenge")
                                    .font(.system(size: 24, weight: .semibold))
                                    .foregroundColor(.white)
                                Spacer()
                            }
                        }
                        .padding(.horizontal, 16)
                        .buttonStyle(PrimaryButtonStyle())
                    }//: navigation link
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .background(
                    Image("pullups")
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .overlay(Color.black.opacity(0.4))
                        .frame(width: proxy.size.width)
                        .edgesIgnoringSafeArea(.all)
                )//.edgesIgnoringSafeArea(.all)
            }// : Geometry reader
            .accentColor(.primary)
        }//: Navigation
    }
}

struct LandingView_Previews: PreviewProvider {
    static var previews: some View {
        LandingView()
    }
}
