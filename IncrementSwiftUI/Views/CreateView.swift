//
//  CreateView.swift
//  IncrementSwiftUI
//
//  Created by rafiul hasan on 12/20/20.
//

import SwiftUI

struct CreateView: View {
    //MARK: Properties
    @StateObject var viewModel = CreateChallengeViewModel()
    @State private var isActive = false
    
    var dropdownList: some View {
        ForEach(viewModel.dropdowns.indices, id: \.self) { index in
            DropdownView(viewModel: $viewModel.dropdowns[index])
        }
    }
    
    //MARK: Body
    var body: some View {
        ScrollView {
            VStack {
                dropdownList
                Spacer()
                NavigationLink(destination: RemaindView(), isActive: $isActive) {
                    Button(action: {
                        //Action
                        isActive = true
                    }) {
                        Text("Next").font(.system(size: 24, weight: .medium))
                    }
                }
            }
            .actionSheet(isPresented: Binding<Bool>(get: {
                viewModel.hasSelectedDropdown
            }, set:{_ in})) { () -> ActionSheet in
                customActionSheet()
            }
            .navigationBarTitle("Create")
            .navigationBarBackButtonHidden(true)
            .padding(.bottom, 16)
        }
    }
    
    func customActionSheet() -> ActionSheet {
        ActionSheet(title: Text("Select"), buttons: viewModel.displayedOptions.indices.map{ index in
            let option = viewModel.displayedOptions[index]
            return ActionSheet.Button.default(Text(option.formatted)) {
                viewModel.send(action: .selectOption(index: index))
            }
        })
    }
}

//struct CreateView_Previews: PreviewProvider {
//    static var previews: some View {
//        CreateView()
//            .previewLayout(.device)
//    }
//}
