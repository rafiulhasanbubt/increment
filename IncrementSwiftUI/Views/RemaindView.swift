//
//  RemaindView.swift
//  IncrementSwiftUI
//
//  Created by rafiul hasan on 12/20/20.
//

import SwiftUI

struct RemaindView: View {
    var body: some View {
        VStack {
            Spacer()
            //DropdownView(viewModel: <#Binding<_>#>)
            Spacer()
            Button(action: {
                //Action
                
            }) {
                Text("Create")
                    .font(.system(size: 24, weight: .medium))
                    .foregroundColor(.primary)
            }.padding(.bottom, 15)
            
            Button(action: {
                //Action
                
            }) {
                Text("Skip")
                    .font(.system(size: 24, weight: .medium))
                    .foregroundColor(.primary)
            }
        }.navigationBarTitle("Remaind")
        .padding(.bottom, 16)
    }
}

struct RemaindView_Previews: PreviewProvider {
    static var previews: some View {
        RemaindView()
            .previewLayout(.device)
    }
}
