//
//  ColorExtension.swift
//  IncrementSwiftUI
//
//  Created by rafiul hasan on 12/20/20.
//

import SwiftUI

extension Color {
    static let darkPrimaryButton = Color("darkPrimaryButtonColor")//(red: 106/255, green: 157/255, blue: 194/255, opacity: 0.3)
    static let primaryButtonColor = Color("primaryButtonColor")
}
